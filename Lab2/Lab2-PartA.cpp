#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    cout << "enter the length for x distance: ";
    double x_distance;
    cin>> x_distance;
    
    cout << "Enter the length for y distance: ";
    double y_distance;
    cin >> y_distance;
    
    cout <<"enter the length of segment 1: ";
    double segment1_length;
    cin >> segment1_length;
    
    cout <<"enter the speed on the road in KMPH: ";
    double segment1_speed;
    cin >> segment1_speed;
    
    cout <<"enter the speed off the road in KMPH: ";
    double segment2_speed;
    cin >> segment2_speed;
    
    double segment2_length;
    segment2_length = sqrt((y_distance - segment1_length)*(y_distance - segment1_length) + (x_distance)*(x_distance));
    
    double total_time, segment1_time, segment2_time;
    
    segment2_time = segment2_length/segment2_speed;
    segment1_time = segment1_length/segment1_speed;
    
    total_time = segment1_time + segment2_time;
    
    cout << "segment1_time: "<<segment1_time<<endl;
    cout << "segment2_time: "<<segment2_time<<endl;
    cout << "Arrival Time: "<<total_time<<endl;
    
    return 0;
}