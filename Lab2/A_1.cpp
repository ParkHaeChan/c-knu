#include <iostream>

using namespace std;

int main()
{
    //letter paper size in inches
    const double letter_width = 8.5;
    const double letter_hight = 11;
    
    //1inch = 25.4millimeters
    const double inch_to_milli = 25.4;
    
    double letter_width_mm = letter_width*inch_to_milli;
    double letter_hight_mm = letter_hight*inch_to_milli;
    
    cout << "letter_width_mm : "<<letter_width_mm<<endl;
    cout << "letter_hight_mm : "<<letter_hight_mm<<endl;
    
    return 0;
}