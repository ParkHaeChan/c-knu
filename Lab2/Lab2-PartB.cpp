#include <iostream>

using namespace std;

int main()
{
    const int FIRST_CLASS_STAMP_PRICE = 44;
    cout <<"Enter number of dollars: ";
    int dollars = 0;
    cin >> dollars;
    
    int first_class_stamps = 100 * dollars / FIRST_CLASS_STAMP_PRICE;
    int penny_stamps = 100 * dollars - first_class_stamps * FIRST_CLASS_STAMP_PRICE;
    
    cout << "the first_class = " << first_class_stamps << endl
         << "the penny_stamps = "<<penny_stamps<<endl;
    
    return 0;
}