#include <iostream>

using namespace std;

int main()
{
    cout<<"Enter value : ";
    int value;
    cin>>value;
    
    cout<<"Enter item price in pennies: ";
    int item_price;
    cin>>item_price;
    
    int value_in_penny = value*100;
    
    int dollar_coin = (value_in_penny-item_price)/100;
    int quaters = ((value_in_penny-item_price)-dollar_coin*100)/25;
    
    cout<<"Dollar coins : "<<dollar_coin<<endl;
    cout<<"Quaters : "<<quaters<<endl;
    
    return 0;
}
    