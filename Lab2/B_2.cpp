#include <iostream>

using namespace std;

int main()
{
    cout<<"enter the cost of a new car : ";
    int car_price;
    cin>>car_price;
    cout<<"enter the estimated miles driven per year : ";
    int mile_per_year;
    cin>>mile_per_year;
    cout<<"enter the estimated gas price : ";
    int gas_price;
    cin>>gas_price;
    cout<<"enter the resale value after 5 years : ";
    int resale_value;
    cin>>resale_value;
    
    int total_price;
    
    total_price = car_price + mile_per_year*5*gas_price-resale_value;
    
    cout<<"total_price : "<<total_price<<endl;
    
    return 0;
}