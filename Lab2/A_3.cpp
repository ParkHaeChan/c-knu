#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    cout<<"type the number : ";
    double number;
    cin >> number;
    
    cout << "square : "<<pow(number, 2)<<endl;
    cout << "cube : "<<pow(number, 3)<<endl;
    cout << "fourth power : "<<pow(number, 4)<<endl;
   
    return 0;
}