#include <iostream>

using namespace std;

void reverse(double* a, int size)
{
    int index = 0;
    double temp[size];
    for(int i=0; i<size; i++)
    {
        temp[i]=a[i];
    }
    for(int i=size-1; i>=0; i--)
    {
        a[index]=temp[i];
        index++;
    }
}

int main()
{
    double ary[10]= {1,2,3,4,5,6,7,8,9,10};
    
    reverse(ary, 10);
    
    for(int i=0; i<10; i++)
    {
        cout<<ary[i]<<" ";
    }
    
    return 0;
}