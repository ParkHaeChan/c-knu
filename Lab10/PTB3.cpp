#include <iostream>
#include <vector>

using namespace std;

void reverse(char s[])
{
    vector<char> stack;
    for(int i=0; s[i]!='\0'; i++)
    {
        stack.push_back(s[i]);
    }
    for(int i=0; s[i]!='\0'; i++)
    {
        s[i]=stack.back();
        stack.pop_back();
    }
}

int main()
{
    char str[] = "Test String";
    
    cout<<str<<endl;
    
    reverse(str);
    
    cout<<str<<endl;
    
    return 0;
}