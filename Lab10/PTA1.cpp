#include <iostream>

using namespace std;

void sort2(double* p1, double* p2)
{
    double temp;
    
    if(*p1 > *p2)
    {
        temp = *p1;
        *p1 = *p2;
        *p2 = temp;
    }
}

int main()
{
    double x=230, y=20;
    
    cout<<"x: "<<x<<endl;
    cout<<"y: "<<y<<endl;
    
    sort2(&x, &y);
    
    cout<<"x: "<<x<<endl;
    cout<<"y: "<<y<<endl;
    
    return 0;
}