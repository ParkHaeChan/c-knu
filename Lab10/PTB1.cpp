#include <iostream>
#include <string>

using namespace std;

string strncpy1(string str, int n)
{
    int i;
    char copy[n];

    for(i=0; i<n; i++)
    {
        copy[i] = str[i];
    }
    copy[i] = '\0';
    
    return copy;
}

int main()
{
    const char str[] = "Copy this sentence!";
    
    cout<<str<<endl;
    cout<<strncpy1(str, 9)<<endl;
    
    return 0;
}