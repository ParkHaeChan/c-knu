#include <iostream>

using namespace std;

int main()
{
    cout<<"a: the smallest and largest of the input"<<endl;
    int sm, lg, input;
    cin>>input;
    sm=input;
    lg=input;
    while(cin>>input)
    {
        if(input == -1)
        {
            break;
        }
        if(sm > input)
        {
            sm = input;
        }
        if(lg < input)
        {
            lg = input;
        }
    }
    cout<<"smallest = "<<sm<<" largest = "<<lg<<endl;
    
    cout<<"b: The num of odd and even inputs"<<endl;
   int odd=0, even=0;
   
    while(cin>>input)
    {
        if(input == -1)
        {
            break;
        }
        if(input%2 == 0)
        {
            even++;
        }
        else
        {
            odd++;
        }
    }
    cout<<"odd = "<<odd<<" even = "<<even<<endl;
    
    cout<<"c: cumulative totals"<<endl;
    int sum = 0;
    while(cin>>input)
    {
        if(input == -1)
        {
            break;
        }
        sum += input;
        cout<<sum<<" ";
    }
    cout<<endl;
    
    cout<< "d: all adjacent duplicates"<<endl;
    int counter=0, temp;
    int ary[100];
    cin>>input;
    temp = input;
    while(cin>>input)
    {
        if(input == -1)
        {
            break;
        }
        if(temp == input)
        {
            ary[counter]=input;
            counter++;
        }
        temp = input;
    }
    temp = 0;
    while(counter!=temp)
    {
        cout<<ary[temp]<<" ";
        temp++;
    }
    cout<<endl;
    
    return 0;
}