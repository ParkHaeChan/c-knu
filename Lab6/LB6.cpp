#include <iostream>

using namespace std;

int main()
{
    cout<<"Enter an interest rate(%): "<<endl;
    double rate;
    cin>>rate;
    
    cout<<"Enter initial amount: "<<endl;
    double initial_balance;
    cin>>initial_balance;
    
    cout<<"Enter target amount: "<<endl;
    double target;
    cin>>target;
    
    double balance = initial_balance;
    
    int year=0;
    
    while(balance < target)
    {
        balance += (balance/100)*rate;
        year++;
    }
    
    cout << "The investment doubled after "<<year<<"years"<<endl;
    cout << "balance : "<<balance<<endl;
    
    return 0;
}