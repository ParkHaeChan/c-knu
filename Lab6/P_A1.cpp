#include <iostream>

using namespace std;

int main()
{
    cout<<"a: The sum of all even numbers 2~100"<<endl;
    int sum=0;
    for(int i=2; i<=100; i+=2)
    {
        sum += i;
    }
    cout<<"sum = "<<sum<<endl;
    
    cout<<"b: The sum of all squares between 1 and 100"<<endl;
    sum=0;
    for(int i=1; i<=100; i++)
    {
        sum += i;
    }
    cout << "sum = "<<sum<<endl;
    
    cout<<"c: All powers of 2 from 20 up to 220"<<endl;
    int pow_of_2 = 2;
    while(pow_of_2 < 220)
    {
        cout<<pow_of_2<<" ";
        pow_of_2 *= 2;
    }
    cout<<endl;
    
    cout<< "d: The sum of all odd numbers between a and b, where a and b are inputs"<<endl;
    int a,b;
    cout<<"a: ";
    cin>>a;
    cout<<"b: ";
    cin>>b;
    sum =0;
    for(a; a<=b; a++)
    {
        if(a%2 == 0)
        {
            continue;
        }
        sum += a;
    }
    cout<<"sum = "<<sum<<endl;
    
    cout<<"e: the sum of all odd digits of an input."<<endl;
    int num, container;
    cout<<"input num : ";
    cin>>num;
    sum=0;
    while(num>1)
    {
        container = num%10;
        if(container%2 == 1)
        {
            sum += container;
        }
        num /= 10;
    }
    cout<<"sum = "<<sum<<endl;
    
    return 0;
}

/*
loop 1 -> execute 5times 5 4 3 2 1

loop 2 -> INF 5 6 7 ...

loop 3 -> not execute while loop

loop 4 -> not execute while loop

loop 5 -> ;->bug
*/