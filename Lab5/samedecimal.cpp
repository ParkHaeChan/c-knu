#include <iostream>

using namespace std;

int main()
{
    float n1,n2;
    
    cout<<"input 2 float nums: ";
    cin>>n1;
    cin>>n2;
    
    if( n1- n2 < 0.001)
    {
        cout<<"They are the same up to two decimal places."<<endl;
    }
    else
    {
        cout<<"They are different."<<endl;
    }
    
    return 0;
}