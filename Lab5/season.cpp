#include <iostream>

using namespace std;

int main()
{
    int month, date;
    string season;
    
    cout<<"input month: ";
    cin >> month;
    cout<<"input date: ";
    cin >> date;
    
    if(month <= 3)
    {
        season = "Winter";
    }
    else if(month <= 6)
    {
        season = "Spring";
    }
    else if(month <= 9)
    {
        season = "Summer";
    }
    else
    {
       season = "Fall";
    }
    
    if((month%3 == 0) && (date >= 21))
    {
        if(season == "Winter")
        {
            season = "Spring";
        }
        else if(season == "Spring")
        {
            season = "Summer";
        }
        else if(season == "Summer")
        {
            season = "Fall";
        }
        else
        {
            season = "Winter";
        }
    }
    
    cout<<season<<endl;
    
    return 0;
}