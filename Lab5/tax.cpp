#include <iostream>

using namespace std;

int main()
{
    int tax, income;
    cout<<"input income :";
    cin >> income;
    
    if(income <= 50000)
    {
        tax = income/100;
    }
    else if( income <= 75000)
    {
        tax = income/100*2;
    }
    else if(income <= 100000)
    {
        tax = income/100*3;
    }
    else if(income <= 250000)
    {
        tax = income/100*4;
    }
    else if(income <= 500000)
    {
        tax = income/100*5;
    }
    else
    {
        tax = income/100*6;
    }
    
    cout<<"your tax will be "<<tax<<endl;
    
    return 0;
}