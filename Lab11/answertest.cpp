/*
codebreaker.cpp
14 july 16 thursday
*/

#include <iostream>
#include <cstdlib>
#include <ctime>


using namespace std;

//constant values
const int SIZE = 5;
const int LIMIT = 5;

void show_menu()
{
    cout<<endl<<"*************************************"<<endl;
    cout<<"Welcome to CODE BREAKER! select Menu"<<endl;
    cout<<"1. Guess"<<endl;
    cout<<"2. Review Guesses"<<endl;
    cout<<"3. New game"<<endl;
    cout<<"4. Quit game"<<endl;
    cout<<"*************************************"<<endl;
}

void print_result(int order, int** ary, int correct_num, int correct_pos)
{
    cout<<"Guess "<<order<<": ";

    for(int i=0; i<SIZE; i++)
    {
        cout<<ary[order-1][i]<<",";
    }
    cout<<"\b: "<<correct_num<<" numbers correct, "<<correct_pos<<" in right position."<<endl;
}

int main()
{
    srand(time(0));
    
    //array memory setting
    //1D array
    int* answer = new int[SIZE];
    //2D array
    int** user_input = new int*[LIMIT];
    for(int i=0; i<LIMIT; i++)
    {
        user_input[i] = new int[SIZE];
    }
    
    //make random answer array
    for(int i = 0; i<SIZE; i++)
    {
        answer[i] = rand()%10;
        cout<<answer[i]<<" ";
    }
    cout<<endl;
    
    //compare user input and password check numbers of correct
    int game_play = 0;
    
    while(true)
    {
        
        //game over
        if(game_play >= LIMIT)
        {
            cout<<"U need more practice!"<<endl;
            break;
        }
        
        int menu;
        show_menu();
        cout<<"Your choice: ";
        cin>>menu;
        if(cin.fail())
        {
            cout<<"Input Error"<<endl;
            exit(1);
        }
        
        //counters
        int right_position=0;
        int numbers_correct=0;
        
        switch(menu)
        {
            case 1:
                //get 5 user inputs
                game_play++;
                cout<<"Input 5 numbers (0~9)"<<endl;
                
                for(int col=0; col<SIZE; col++)
                {
                    cin>>user_input[game_play-1][col];
                    //check input range error
                    if(user_input[game_play-1][col] > 9 || user_input[game_play-1][col] < 0)
                    {
                        cout<<"Input range error"<<endl;
                        exit(1);
                    }
                }
                
                //check right position
                for(int i =0; i<SIZE; i++)
                {
                    if(answer[i] == user_input[game_play-1][i])
                    {
                        right_position++;
                    }
                }
                
                //check numbers_correct
                for(int i =0; i<SIZE; i++)
                {
                    for(int j=0; j<SIZE; j++)
                    {
                        if(answer[i] == user_input[game_play-1][j])
                        {
                            numbers_correct++;
                            break;
                        }
                    }
                }
                
                //print result
                print_result(game_play, user_input, numbers_correct, right_position);
                
                if(right_position == SIZE)
                {
                    //user win the game
                    cout<<"user win!"<<endl;
                    exit(0);
                }
            break;
            
            case 2:
                if(game_play == 0)
                {
                    cout<<"Empty inputs. Guess numbers first"<<endl;
                }
                for(int rv=1; rv<=game_play; rv++)
                {
                    print_result(rv, user_input, numbers_correct, right_position);
                }
            break;
            
            case 3:
                cout<<"Start New Game"<<endl;
                //new game set
                for(int i = 0; i<SIZE; i++)
                {
                    //remake answer
                    answer[i] = rand()%10;
                    //cout<<answer[i]<<" ";
                }
                game_play = 0;
                continue;
            break;
            
            case 4:
            //quit game
            if(menu == 4)
            {
                cout<<"Quit Game"<<endl;
                exit(0);
            }
            break;
            
            default:
                cout<<"Input error (only 1~4 integer)"<<endl;
            break;
        }
    }
    
    //free memory
    for(int i=0; i<LIMIT; i++)
    {
        delete[] user_input[i];
    }
    delete[] user_input;
    delete[] answer;
    
    return 0;
}