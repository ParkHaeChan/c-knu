#include <string>

using namespace std;

int count_words(string str)
{
    int length = 0;
    
    for(int i=0; str[i]; i++)
    {
        length++;
    }
    
    return length;
}