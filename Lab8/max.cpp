#include <iostream>

using namespace std;

double max(double input1, double input2)
{
    if(input1> input2)
    {
        return input1;
    }
    else
    {
        return input2;
    }
}

int main()
{
    double input[3];
    cout<<"Enter three number: ";
    for(int i=0; i<3; i++)
    {
        cin>>input[i];
    }
    double first_two = max(input[0], input[1]);
    cout<<"the larger of the first two input: "<<first_two<<endl;
    
    double last_two = max(input[1], input[2]);
    cout<<"the larger of the last two input: "<<last_two<<endl;
    
    cout<<"the largest of the all three input: "<<max(first_two, last_two)<<endl;
    
    return 0;
}