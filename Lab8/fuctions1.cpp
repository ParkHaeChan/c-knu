#include <iostream>

using namespace std;

/*
* computes a Volume of a cube
* @param side_length of the side of a cube
* @return the volume
*/
double cube_volume(double side_length)
{
    double volume = side_length*side_length*side_length;
    return volume;
}

int main()
{
    double side_length = 2;
    double volume = side_length * side_length * side_length;
    
    cout<<" A cube with side_length "<<side_length<<" is = "<<volume<<endl;
    
    side_length = 10;
    double volume1 = side_length * side_length * side_length;
    
    cout<<" A cube with side_length "<<side_length<<" is = "<<volume1<<endl;
    
    double result1 = cube_volume(2);
    double result2 = cube_volume(10);
    
    cout<<" A cube with side_length "<<side_length<<" is = "<<result1<<endl;
    cout<<" A cube with side_length "<<side_length<<" is = "<<result2<<endl;
    
    for(int i=1; i<=10; i++)
    {
        double result = cube_volume(i);
        cout<<" A cube with side length " <<i<<" is = "<<result<<endl;
    }
    
    return 0;
    
}