#include <iostream>
#include <string>
#include "functions.h"

using namespace std;

int main()
{
    cout<<"Input string: ";
    string str;
    getline(cin, str);
    
    cout<<"Length of string: "<<count_words(str)<<endl;
    
    return 0;
}