#include<iostream>

using namespace std;

int main()
{
    int ary[10];
    int mul = 1;
    
    for(int i=0; i<10; i++)
    {
        ary[i] = i+1;
        
        mul *= ary[i];
    }
    
    cout<<"10! = "<<mul<<endl;
    
    return 0;
}