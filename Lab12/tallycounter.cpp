#include <iostream>

using namespace std;

class Rectangle
{
    public:
    
    Rectangle(double w = 1, double h = 2) : height(h), length(w)
    {
        
    }
    
    double get_perimeter() const;
    double get_area() const;
    double get_width() const;
    double get_length() const;
    
    void set_length(double L);
    void set_width(double W);
    void resize(double L, double W);
    
    double length;
    double height;
};

double Rectangle::get_perimeter() const
{
    
}

double Rectangle::get_area() const
{
    return length*height;
}

double Rectangle::get_width() const
{
    return length;
}

double Rectangle::get_length() const
{
    return height;
}

void Rectangle::set_length(double L)
{
    height = L;
}

void Rectangle::set_width(double W)
{
    length = W;
}

void Rectangle::resize(double L, double W)
{
    length = W;
    height = L;
}

int main()
{
    Rectangle rect;
    
    rect.set_length(10);
    rect.set_width(5);
    
    cout<<rect.get_area()<<endl;
    cout<<rect.get_length()<<endl;
    cout<<rect.get_width()<<endl;
    
    return 0;
}