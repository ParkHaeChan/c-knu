#include <iostream>
#include <iomanip>

using namespace std;

/*
* A simulated cash register that tracks the item
* count and the total amount due
*/

class CashRegister
{
    public:
    //mutators
    
    /*
    *clears the register item count and total
    */
    void clear();
    /*
    * adds an item to this cash register
    * @param price the price of this item
    */
    void add_item(double price);
    
    //Accessors Member Functions
    
    //@return return the total amount of the current sale
    double get_total() const;
    int get_count() const;
    
    private:
    //data members
    int item_count;
    double total_price;
    
};

void CashRegister::clear()
{
    item_count = 0;
    total_price = 0;
}

void CashRegister::add_item(double price)
{
    item_count++;
    total_price = total_price + price;
}

double CashRegister::get_total() const
{
    return total_price;
}

int CashRegister::get_count() const
{
    return item_count;
}

void display(CashRegister reg)
{
    cout<<"count: "<<reg.get_count()<<endl;
    cout<<"total: $"<<reg.get_total()<<endl;
}

int main()
{
    CashRegister register1;
    register1.clear();
    register1.add_item(1.95);
    display(register1);
    
    return 0;
}

