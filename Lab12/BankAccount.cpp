//BankAccount Class
#include <iostream>

using namespace std;

class BankAccount
{
    public:
        BankAccount();
        BankAccount(double initial_balance);
        
        void deposit(double amount);
        void withdraw(double amount);
        void add_interest(double rate);
        
        double get_balance() const;
        
    private:
        double balance;
};

double BankAccount::get_balance() const
{
    return balance;
}

void BankAccount::deposit(double amount)
{
    balance = balance + amount;
}

void BankAccount::withdraw(double amount)
{
    const double PENALTY = 10;
    if(amount>balance)
    {
        balance = balance - PENALTY;
    }
    else
    {
        balance = balance - amount;
    }
}

void BankAccount::add_interest(double rate)
{
    double amount = balance * rate / 100;
    deposit(amount);
}

BankAccount::BankAccount()
{
    balance = 0;
}

BankAccount::BankAccount(double initial_balance)
{
    balance = initial_balance;
}

int main()
{
    BankAccount* park = new BankAccount(1000);
    BankAccount* lee = new BankAccount(500);
    
    cout<<"Park: "<<park->get_balance()<<endl;
    cout<<"Lee: "<<lee->get_balance()<<endl;
    
    cout<<"After add interest"<<endl;
    
    park->add_interest(10);
    
    lee->add_interest(15);
    
    cout<<"Park: "<<park->get_balance()<<endl;
    cout<<"Lee: "<<lee->get_balance()<<endl;
    
    return 0;
}