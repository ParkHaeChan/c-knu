#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>

using namespace std;

const int TRUE = 1;
const int FALSE = 0;

int main()
{
    string input;
    
    //input status
    int isPalin = TRUE; //start with TRUE
    
    cout<<"Enter a palindrom: ";
    //get user input
    getline(cin, input);
    
    cout<<"Do you want to ignore case? ";
    char casecheck;
    cin>>casecheck;
    
    if(casecheck == 'Y' || casecheck == 'y')
    {
        //ignore case
        for(int i = 0; i<input.length()/2; i++)
        {
            if(input.at(i)>=65 && input.at(i)<91) // large case: ABC...
            {
                if(!(input.at(i) == input.at(input.length()-1-i)-32 || input.at(i) == input.at(input.length()-1-i)))
                {
                    isPalin = FALSE; //if not palindrome state change to FALSE
                }
            }
            else if(input.at(i)>=97 && input.at(i)<123) // small case: abc...
            {
                if(!(input.at(i) == input.at(input.length()-1-i)+32 || input.at(i) == input.at(input.length()-1-i)))
                {
                    isPalin = FALSE; //if not palindrome state change to FALSE
                }
            }
            else    //numerical cases
            {
                if(input.at(i) != input.at(input.length()-1-i))
                {
                    isPalin = FALSE; //if not palindrome state change to FALSE
                }
            }
        }
    }
    else if(casecheck == 'N' || casecheck == 'n')
    {
        //not ignore
        for(int i = 0; i<input.length()/2; i++)
        {
            if(input.at(i) != input.at(input.length()-1-i))
            {
                isPalin = FALSE; //if not palindrome state change to FALSE
            }
        }
    }
    else
    {
        cout<<"Input error"<<endl;
        exit(1);
    }
    
    //print whether palindrome or not
    if(isPalin)
    {
        cout<<"This is a valid palindrom"<<endl;
    }
    else
    {
        cout<<"This is not a valid palindrom"<<endl;
    }
    
    return 0;
}