#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>

using namespace std;

int main()
{
    int count = 0;
    
    cout<<"Enter a word: ";
    string word;
    cin >> word;
    
    for(int i = 0; i<word.length(); i++)
    {
        string letter = word.substr(i,1);
        if(letter == "a" || letter == "A")
        {
            count++;
        }
    }
    
    cout<<"There was "<< count << " letters of type a" << endl;
    
    return 0;
}