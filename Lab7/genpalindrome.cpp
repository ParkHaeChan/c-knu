#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>

using namespace std;

int main()
{
    int length;
    
    srand(time(0));
    
    cout<<"Enter length of palindrome: ";
    cin>>length;
    
    string str1, str2;
    
    //separate 2 cases even length, odd length
    
    if(length%2 == 0)   //even
    {
        for(int i = 0; i<length/2; i++)
        {
            int asciiVal;
            
            switch(rand()%3+1)
            {
                 //rand()%26 + 97 abc...
                case 1: asciiVal = rand()%26 + 97; break;
                
                //rand()%26 + 65 ABC...
                case 2: asciiVal = rand()%26 + 65; break;
                
                //rand()%10 + 48 123...
                case 3: asciiVal = rand()%10 + 48; break;
            }
            
            char asciiChar = asciiVal;
            
            str1.insert(i, 1, asciiChar);
            str2.insert(i, 1, asciiChar);
        }
    }
    else   //odd
    {
        for(int i = 0; i<length/2+1; i++)
        {
            int asciiVal;
            
            switch(rand()%3+1)
            {
                 //rand()%26 + 97 abc...
                case 1: asciiVal = rand()%26 + 97; break;
                
                //rand()%26 + 65 ABC...
                case 2: asciiVal = rand()%26 + 65; break;
                
                //rand()%10 + 48 123...
                case 3: asciiVal = rand()%10 + 48; break;
            }
            
            char asciiChar = asciiVal;
            
            str1.insert(i, 1, asciiChar);
            if(i!=length/2) //don't add last(overlap avoid)
                str2.insert(i, 1, asciiChar);
        }
    }
    
    //reverse str2
    for(int i = 0; i<str2.length()/2; i++)
    {
        char temp = str2.at(i);
        str2.at(i) = str2.at(str2.length()-1-i);
        str2.at(str2.length()-1-i) = temp;
    }
    
    //concat str1 and str2
    str1 = str1+str2;
    
    cout<<str1<<endl; //print palindrome
    
    return 0;
}