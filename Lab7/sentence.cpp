#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>

using namespace std;

int main()
{
    int countA=0, countE=0, countI=0, countO=0, countU = 0;
    
    cout<<"Enter a sentence: ";
    string sentence;
    getline(cin, sentence);
    
    for(int i = 0; i<sentence.length(); i++)
    {
        string letter = sentence.substr(i,1);
        if(letter == "a" || letter == "A")
        {
            countA++;
        }
        if(letter == "e" || letter == "E")
        {
            countE++;
        }
        if(letter == "i" || letter == "I")
        {
            countI++;
        }
        if(letter == "o" || letter == "O")
        {
            countO++;
        }
        if(letter == "u" || letter == "U")
        {
            countU++;
        }
    }
    
    cout<<"There was "<< countA << " letters of type a" << endl;
    cout<<"There was "<< countE << " letters of type e" << endl;
    cout<<"There was "<< countI << " letters of type i" << endl;
    cout<<"There was "<< countO << " letters of type o" << endl;
    cout<<"There was "<< countU << " letters of type u" << endl;
    
    return 0;
}