//BankAccount.h
#ifndef BANKACCOUNT_H
#define BANKACCOUNT_H
#include <vector>
using namespace std;

class BankAccount
{
    public:
        BankAccount();
        BankAccount(double initial_balance);
        
        void deposit(double amount);
        void withdraw(double amount);
        void add_interest(double rate);
        void show_history();
        
        double get_balance() const;
        
    private:
        double balance;
        vector<double> history;
};

#endif