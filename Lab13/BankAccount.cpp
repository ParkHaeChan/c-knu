//BankAccount.cpp
#include "BankAccount.h"
#include <iostream>
using namespace std;

double BankAccount::get_balance() const
{
    return balance;
}

void BankAccount::deposit(double amount)
{
    balance = balance + amount;
    history.push_back(balance);
}

void BankAccount::withdraw(double amount)
{
    const double PENALTY = 10;
    if(amount>balance)
    {
        balance = balance - PENALTY;
        history.push_back(balance);
    }
    else
    {
        balance = balance - amount;
        history.push_back(balance);
    }
}

void BankAccount::add_interest(double rate)
{
    double amount = balance * rate / 100;
    deposit(amount);
}

void BankAccount::show_history()
{
    for(int i=0; i<history.size(); i++)
    {
        cout<<i<<": "<<history.at(i)<<endl;
    }
}

BankAccount::BankAccount()
{
    balance = 0;
    history.push_back(0);
}

BankAccount::BankAccount(double initial_balance)
{
    balance = initial_balance;
    history.push_back(initial_balance);
}
