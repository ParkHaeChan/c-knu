#include <iostream>
#include "BankAccount.h"
using namespace std;

int main()
{
    BankAccount* park = new BankAccount(1000);
    BankAccount* lee = new BankAccount(500);
    
    cout<<"Park: "<<park->get_balance()<<endl;
    cout<<"Lee: "<<lee->get_balance()<<endl;
    
    cout<<"After add interest"<<endl;
    
    park->add_interest(15);
    lee->add_interest(15);
    
    cout<<"Park: "<<park->get_balance()<<endl;
    cout<<"Lee: "<<lee->get_balance()<<endl;
    
    cout<<"After withdraw"<<endl;
    park->withdraw(100);
    lee->withdraw(500);
    
    cout<<"Park: "<<park->get_balance()<<endl;
    cout<<"Lee: "<<lee->get_balance()<<endl;
    
    cout<<"After deposit"<<endl;
    park->deposit(1000);
    lee->deposit(100);
    
    cout<<"Park: "<<park->get_balance()<<endl;
    cout<<"Lee: "<<lee->get_balance()<<endl;
    
    cout<<"show history Park: "<<endl;
    park->show_history();
    cout<<"show history Lee: "<<endl;
    lee->show_history();
    
    return 0;
}