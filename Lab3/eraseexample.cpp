#include <iostream>
#include <string>

using namespace std;

int main()
{
    string text = "hello world, this is a test";
    
    string fragment1 = text.substr(6,5);    // start at pos 6 and take 5 chars
    string fragment2 = text.substr(3);      // start at pos 3 and take all chars to the endl
    //string fragment3 = text.substr(28,2);   // start at pos 28 and take 2 chars(error if string is no longer than 28)
    string fragment4 = text.substr(2,30);   // start at pos 2 and take 30 chars(or limit ==> not over reading)
    string fragment5 = text;
    
    fragment5.erase(0,5); //erase 5chars ==> "hello"
    string fragment6 = text.replace(18,2,"was");    //replace is with was
    
    cout << "Original: "<<text<<endl;
    cout << "line1: " << fragment1 << endl;
    cout << "line2: " << fragment2 << endl;
    //cout << "line3: " << fragment3 << endl;
    cout << "line4: " << fragment4 << endl;
    cout << "line5: " << fragment5 << endl;
    cout << "line6: " << fragment6 << endl;
    
    return 0;
}