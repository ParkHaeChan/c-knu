# include <iostream>
#include <math.h>

using namespace std;

int main()

{

    char o;

    double num1,num2;

    cout << "Operator: ";

    cin >> o;
    
    double func;
    double Td;

    switch(o) 
    {

    case '+':
        cout << "Num1: ";
        cin >> num1;
        cout << "Num2: ";
        cin >> num2;

        // num1+num2;
        cout<<"Result: "<<num1<<" + "<<num2<<" = "<<num1+num2<<endl;

        break;

    case '-':
        cout << "Num1: ";
        cin >> num1;
        cout << "Num2: ";
        cin >> num2;

        // num1-num2;
        cout<<"Result: "<<num1<<" - "<<num2<<" = "<<num1-num2<<endl;

        break;
        
    case '*':
        cout << "Num1: ";
        cin >> num1;
        cout << "Num2: ";
        cin >> num2;
    
        // num1*num2;
        cout<<"Result: "<<num1<<" * "<<num2<<" = "<<num1*num2<<endl;

        break;
        
    case '/':
        cout << "Num1: ";
        cin >> num1;
        cout << "Num2: ";
        cin >> num2;
    
        // num1/num2;
        cout<<"Result: "<<num1<<" / "<<num2<<" = "<<num1/num2<<endl;

        break;
        
    case '^':
        cout << "Num1: ";
        cin >> num1;
        cout << "Num2: ";
        cin >> num2;
    
        // num1^num2;
        cout<<"Result: "<<num1<<" ^ "<<num2<<" = "<<pow(num1, num2)<<endl;

        break;
        
    case 'R':
        cout << "Num1: ";
        cin >> num1;
        cout << "Num2: ";
        cin >> num2;
    
        // num1_th root of num2;
        cout<<"Result: "<<num1<<"_th root of "<<num2<<" = "<<pow(num1, 1/num2)<<endl;

        break;
        
    case 'S':
        cout << "Num1: ";
        cin >> num1;
    
        // sin(num1*PI/180);
        cout<<"Result: sin("<<num1<<") ="<<sin(num1*3.141592/180)<<endl;

        break;
    
    case 'L':
        cout << "Num1: ";
        cin >> num1;
    
        // Log(num1);
        cout<<"Result: Log("<<num1<<") = "<<log(num1)/log(10)<<endl;

        break;
        
    case 'H':
    
        // Help
        cout<<"support 12 operators: +,-,*,/,^,R => (Ath root of B), S => (Sin(A)),L => (Log(A)),"<<endl
            <<"H => (Help), C => (volume of cylinder, input: r, h), I => (interest of A_years, principle_B and rate 10%)"<<endl
            <<"D => (Dew point temperature, input: temperature T, relative humidity R(0 to 1)"<<endl;

        break;
        
    case 'C':
        cout << "r: ";
        cin >> num1;
        cout << "h: ";
        cin >> num2;
    
        // volume of cylinder = r*r*PI*h
        cout<<"Result: volume of cylinder = "<<num1*num1*3.141592*num2<<endl;

        break;
        
    case 'I':
        cout << "years: ";
        cin >> num1;
        cout << "Principle: ";
        cin >> num2;
    
        // interest of A_years, principle_B and rate 10%
        cout<<"Result: interest = "<<pow(num2*110/100,num1)<<endl;

        break;
        
    case 'D':
        cout << "T: ";
        cin >> num1;
        cout << "R(0 to 1): ";
        cin >> num2;
        
        func = (17.27*num1)/(237.7+num1)+log(num2)/log(M_E);
        Td = (237.7*func)/(17.27-func);
    
        // Dew point temperature (temperature T(num1) relative humidity R(num2))
        cout<<"Result: Dew Point Temperature = "<<Td<<endl;

        break;

    default:

        /* If operator is not exist in switch-case, error message is shown */
        cout << "Error! operator is not correct";

        break;

    }

    return 0;

}