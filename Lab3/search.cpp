#include <iostream>
#include <string>

using namespace std;

int main()
{
    string text = "hello world, this is a test";
    string textin;
    cout <<"Enter a string"<<endl;
    getline(cin, textin);
    
    if(textin.find("password") != string::npos)
    {
        cout<<"Contains password!"<<endl;
    }
    else
    {
        cout << " does not contain password"<<endl;
    }
    
    return 0;
}