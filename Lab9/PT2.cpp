#include <iostream>

using namespace std;

bool equals(int a[], int a_size, int b[], int b_size)
{
    if(a_size != b_size)
    {
        return false;
    }
    
    for(int i=0; i<a_size; i++)
    {
        if(a[i] != b[i])
        {
            return false;
        }
    }
    
    return true;
}

int main()
{
    const int SIZE = 10;
    int ary1[SIZE] = {1,1,1,1,1,1,1,1,1,1};
    int ary2[SIZE] = {1,1,1,1,1,1,1,1,1,1};
    
    if(equals(ary1, SIZE, ary2, SIZE))
    {
        cout<<"ary1 and ary2 are same"<<endl;
    }
    else
    {
        cout<<"both are not same"<<endl;
    }
}