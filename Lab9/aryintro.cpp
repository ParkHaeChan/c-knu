#include <iostream>

using namespace std;

int main()
{
    int numbers[5];
    const int SIZE = 5;
    int numbers1[SIZE];
    int numbers2[5] = {0,1,2,3,4};
    int numbers3[] = {0, 1, 2, 3, 4};
    int numbers4[5] = {0,1,2};
    
    string names[3]={"I am", "who", "you know"};
    
    for(int i = 0; i<SIZE; i++)
    {
        cout<<names[i]<<endl;
    }
    
    return 0;
}