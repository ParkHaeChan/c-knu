#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    srand(time(0));
    
    const int SIZE = 10;
    int ary[SIZE];
    
    for(int i=0; i<10; i++)
    {
        ary[i] = rand()%10+1;
    }
    
    cout<<"A: even index"<<endl;
    for(int i=0; i<SIZE; i++)
    {
        if(i%2 == 0)
        {
            cout<<"element"<<i<<" : "<<ary[i]<<endl;
        }
    }
    
    cout<<"B: every even element"<<endl;
    for(int i =0; i<SIZE; i++)
    {
        if(ary[i]%2 == 0)
        {
            cout<<"element"<<i<<" : "<<ary[i]<<endl;
        }
    }
    
    cout<<"C: reverse"<<endl;
    cout<<"origin: ";
    for(int i=0; i<SIZE; i++)
    {
        cout<<"element"<<i<<" : "<<ary[i]<<" ";
    }
    cout<<endl<<"reverse: ";
    
    int temp[SIZE];
    
    for(int i=0; i<SIZE; i++)
    {
        temp[i]=ary[SIZE-1-i];
    }
    
    for(int i=0; i<SIZE; i++)
    {
        cout<<"element"<<i<<" : "<<temp[i]<<" ";
    }
    
    cout<<endl<<"D: first and last"<<endl;
    cout<<"first: "<<ary[0]<<endl;
    cout<<"last: "<<ary[SIZE-1]<<endl;
    
    /*
    int last;
    for(int i=0; (ary_element_nums); i++)
    {
        last = ary[i];
    }
     cout<<"last: "<<last<<endl;
     */
}