#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    const int CAPACITY = 10;
    int values[CAPACITY];
    bool found = false;
    int pos = 0;
    int size = 0;
    int input;
    
    cout<<"Enter a series of values with Q to end"<< endl;
    
    while(size < CAPACITY)
    {
        cin>>input;
        if(cin.fail()){break;}
        values[size] = input;
        size++;
    }
    
    sort(values, values+size);
    
    while(pos < size && !found)
    {
        if(values[pos] == 100)
        {
            found = true;
            cout<<"Found at position "<<pos<<endl;
        }
        else
            pos++;
    }
    
    if(!found)
    {
        cout<<"not found "<<endl;
    }
    else
    {
        int temp = values[0];
        values[0] = values[pos];
        values[pos] = temp;
    }
    
    for(int i=0; i<size; i++)
    {
        cout<<"element "<<i<<" = "<<values[i]<<endl;
    }
    
    return 0;
}